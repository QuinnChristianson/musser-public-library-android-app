package com.example.qchri.events;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Today extends AppCompatActivity {
    public String [] iCal;
    public int total_lines;
    String[] months;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today);

        Context th = this.getBaseContext();
        AssetManager am = th.getAssets();

        TextView text = (TextView) findViewById(R.id.events);
        DatePicker date = (DatePicker) findViewById(R.id.date);

        months = new String[] { "January ","February ","March ","April ","May ","June ","July ","August ","September ","October ","November ","December "};

        try {
            InputStream is = am.open("iCall.ics");
            InputStreamReader r = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(r);
            String [] str = new String[1000];
            String line = "";
            int lines = 0;
            if (is != null) {
                while ((line = reader.readLine()) != null) {
                    str[lines] = line;
                    lines++;
                }
            }
            String[] str_final = new String[lines];
            int i = 0;
            total_lines = lines;
            while (i<lines){
                str_final[i] = str[i] + '\n';
                i++;
            }
            iCal = str_final;
        } catch (IOException e) {
        }

        int date_month = (date.getYear()*10000)+((date.getMonth()+1)*100)+(date.getDayOfMonth());
        String t = "DTSTART;VALUE=DATE-TIME:" + String.valueOf(date_month);
        int i = total_lines -1;
        text.setText("");
        String day = "";
        while (i >= 0){
            if(iCal[i].substring(0, 6).equals("DTSTAR")){
                String y = iCal[i].substring(0,32);
                if(y.equals(t)){
                    String u = iCal[i].substring(30,32);
                    if(!u.equals(day)){
                        if(text.getText().equals("") || text.getText().equals("No Events for This Week")){
                            if (u.startsWith("0")){
                                u = iCal[i].substring(31,32);
                            }
                            String date_string=  months[(date.getMonth())] + u;
                            text.append(date_string + '\n');
                            u = iCal[i].substring(30,32);
                            day = u;
                        } else {
                            if (u.startsWith("0")){
                                u = iCal[i].substring(31,32);
                            }
                            String date_string = months[(date.getMonth())] + u;
                            text.append('\n' + date_string + '\n');
                            u = iCal[i].substring(30,32);

                            day = u;
                        }
                    }
                    text.append(" - "+iCal[i+6].substring(8,iCal[i+6].length()));
                }
            }
            i--;
        }
        if(text.getText().equals("")) {
            text.setText("No Events for Today");
        }
    }

    public void create_today (View v) {
        setContentView(R.layout.activity_today);
        TextView text = (TextView) findViewById(R.id.events);
        DatePicker date = (DatePicker) findViewById(R.id.date);
        int date_month = (date.getYear()*10000)+((date.getMonth()+1)*100)+(date.getDayOfMonth());
        String t = "DTSTART;VALUE=DATE-TIME:" + String.valueOf(date_month);
        int i = total_lines -1;
        text.setText("");
        String day = "";
        while (i >= 0){
            if(iCal[i].substring(0, 6).equals("DTSTAR")){
                String y = iCal[i].substring(0,32);
                if(y.equals(t)){
                    String u = iCal[i].substring(30,32);
                    if(!u.equals(day)){
                        if(text.getText().equals("") || text.getText().equals("No Events for This Week")){
                            if (u.startsWith("0")){
                                u = iCal[i].substring(31,32);
                            }
                            String date_string=  months[(date.getMonth())] + u;
                            text.append(date_string + '\n');
                            u = iCal[i].substring(30,32);
                            day = u;
                        } else {
                            if (u.startsWith("0")){
                                u = iCal[i].substring(31,32);
                            }
                            String date_string = months[(date.getMonth())] + u;
                            text.append('\n' + date_string + '\n');
                            u = iCal[i].substring(30,32);
                            day = u;
                        }
                    }
                    text.append(" - "+iCal[i+6].substring(8,iCal[i+6].length()));
                }
            }
            i--;
        }
        if(text.getText().equals("")) {
            text.setText("No Events for Today");
        }
    }

    public void create_this_week (View v) {
        setContentView(R.layout.activity_this_week);
        TextView text = (TextView) findViewById(R.id.events);
        DatePicker date = (DatePicker) findViewById(R.id.date);
        text.setText("");
        int start = date.getDayOfMonth();
        int stop = start+6;

        for(start = date.getDayOfMonth(); start<= stop; start++){
            int date_month = (date.getYear()*10000)+((date.getMonth()+1)*100)+(start);
            String t = "DTSTART;VALUE=DATE-TIME:" + String.valueOf(date_month);
            int i = total_lines -1;
            String day = "";
            while (i >= 0){
                if(iCal[i].substring(0, 6).equals("DTSTAR")){
                    String y = iCal[i].substring(0,32);
                    if(y.equals(t)){
                        String u = iCal[i].substring(30,32);
                        if(!u.equals(day)){
                            if(text.getText().equals("") || text.getText().equals("No Events for This Week")){
                                if (u.startsWith("0")){
                                    u = iCal[i].substring(31,32);
                                }
                                String date_string=  months[(date.getMonth())] + u;
                                text.setText(date_string + '\n');
                                u = iCal[i].substring(30,32);
                                day = u;
                            } else {
                                if (u.startsWith("0")){
                                    u = iCal[i].substring(31,32);
                                }
                                String date_string = months[(date.getMonth())] + u;
                                text.append('\n' + date_string + '\n');
                                u = iCal[i].substring(30,32);
                                day = u;
                            }
                        }
                        text.append(" - "+iCal[i+6].substring(8,iCal[i+6].length()));
                    }
                }
                i--;
            }
            if(text.getText().equals("")) {
                text.setText("No Events for This Week");
            }

        }
    }

    public void create_this_month (View v) {
        setContentView(R.layout.activity_this_month);
        TextView text = (TextView) findViewById(R.id.events);
        DatePicker date = (DatePicker) findViewById(R.id.date);
        text.setText(date.getMonth()+ " " + date.getYear());
        int date_month = (date.getYear()*100)+(date.getMonth()+1);
        String t = "DTSTART;VALUE=DATE-TIME:" + String.valueOf(date_month);
        int i = total_lines -1;
        text.setText("");
        String day = "";
        while (i >= 0){
            if(iCal[i].substring(0, 6).equals("DTSTAR")){
                String y = iCal[i].substring(0,30);
                if(y.equals(t)){
                    String u = iCal[i].substring(30,32);
                    if(!u.equals(day)){
                        if(text.getText().equals("") || text.getText().equals("No Events for This Week")){
                            if (u.startsWith("0")){
                                u = iCal[i].substring(31,32);
                            }
                            String date_string=  months[(date.getMonth())] + u;
                            text.append(date_string + '\n');
                            u = iCal[i].substring(30,32);
                            day = u;
                        } else {
                            if (u.startsWith("0")){
                                u = iCal[i].substring(31,32);
                            }
                            String date_string = months[(date.getMonth())] + u;
                            text.append('\n' + date_string + '\n');
                            u = iCal[i].substring(30,32);
                            day = u;
                        }
                    }
                    text.append(" - "+iCal[i+6].substring(8,iCal[i+6].length()));
                }
            }
            i--;
        }
        if(text.getText().equals("")) {
            text.setText("No Events for This Month");
        }
    }
}


